# Engineering Logbook 02

|Name|Matric No|Subsytems|Logbook|Date|
|-----|-----|-----|-----|-----|
|Che Wan Muhammad Luqman bin Che Wan Mohamad Anan|197589|Flight System Integration|02|05/11/2021|

|Agenda|
|-----|
| 1. Check the current progress of the project at lab and presenting the gantt chart. |
| 2. Update the logboook. |   


|Goals|
|-----|
| To align goals and project progress, and review the task that need to be prioritize. |


|Method and Justification|
|-----|
| Our group presenter, Syahirah aka YB has already presented the gantt chart of our subsystems to other subsystems for they to understand the job scope of our group and what will our group do along the 14 weeks. By explaining the tasks flow and the duration of the tasks going have implant the target set which is align with the milestone of the project. Since our group is insential to other subsystems group, this will help other subsystems group to understand the flow of the project better. As for the logbook, it is just to recorded the track of our project. |

|Impact on the project|
|-----|
| As mentioned above, by presenting the gantt chart of our subsytems group, other subsystems group to understand the flow of the project better. |


|Next Step|
|-----|
| Execute the task given for the subsystems and monitor the work done by other subsystems group. |
